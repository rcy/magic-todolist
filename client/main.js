import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Items } from '../imports/items.js';

import './main.html';

window.Items = Items

Template.app.helpers({
  items() {
    return Items.find({ parent_id: null })
  }
})

const query = new ReactiveVar('')

Template.search.events({
  'keyup'(event, instance) {
    query.set(event.currentTarget.value)
  }
})

Template.add_item.onCreated(function () {
  this.addingItem = new ReactiveVar(false)
})

Template.add_item.events({
  'click button'(event, instance) {
    event.preventDefault()
    instance.addingItem.set(true)
  },
  'change input'(event, instance) {
    const name = event.currentTarget.value
    Items.insert({ name, parent_id: instance.data.parent_id })
    instance.addingItem.set(false)
  },
  'blur input'(event, instance) {
    instance.addingItem.set(false)
  }
})

Template.add_item.helpers({
  adding() {
    return Template.instance().addingItem.get()
  }
})

Template.todo_item.helpers({
  done() {
    return Template.instance().data.item.done
  },
  doneClass() {
    return Template.instance().data.item.done ? 'done' : ''
  },
  matchClass() {
    return !Template.instance().data.item.name.match(query.get()) ? 'match' : ''
  },
  subitems() {
    const item = Template.instance().data.item
    return Items.find({ parent_id: item._id })
  }
})

Template.todo_item.events({
  'click input[type=checkbox]'(event, instance) {
    console.log({ event, instance })
    event.preventDefault()
    event.stopPropagation()
    const item = instance.data.item
    console.log(item, item.name ,item.done)
    Items.update({ _id: item._id }, {$set: { done: !item.done } })
  }
})


/* Template.hello.onCreated(function helloOnCreated() {
 *   // counter starts at 0
 *   this.counter = new ReactiveVar(0);
 * });
 *
 * Template.hello.helpers({
 *   counter() {
 *     return Template.instance().counter.get();
 *   },
 * });
 *
 * Template.hello.events({
 *   'click button'(event, instance) {
 *     // increment the counter when button is clicked
 *     instance.counter.set(instance.counter.get() + 1);
 *   },
 * }); */
